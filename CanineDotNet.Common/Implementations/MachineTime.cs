﻿namespace CanineDotNet.Common.Implementations
{
    using CanineDotNet.Common.Interfaces;
    using System;

    public class MachineTime : IDateTime
    {
        public DateTime UtcNow => DateTime.UtcNow;

        public DateTime Now => DateTime.Now;
    }
}
