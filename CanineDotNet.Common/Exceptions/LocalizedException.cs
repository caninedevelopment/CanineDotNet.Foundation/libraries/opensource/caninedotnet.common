﻿using System;

namespace CanineDotNet.Common.Exceptions
{
    [Serializable]
    public abstract class LocalizedException : Exception
    {
        public LocalizedString Error { get; internal set; }

        protected LocalizedException() { }
        protected LocalizedException(string message) : base(message) { }
        protected LocalizedException(string message, Exception inner) : base(message, inner) { }
        protected LocalizedException(string message, LocalizedString error) : this(message)
        {
            this.Error = error;
        }
    }
}
