﻿using System.Threading.Tasks;

namespace CanineDotNet.Common.Interfaces.Command
{
    public interface IProcedureAsync<TCommandModelIn> : ICommand
        where TCommandModelIn : IDtoRequest
    {
        Task ExecuteAsync(TCommandModelIn input);
    }

    public interface IProcedureAsync : ICommand
    {
        Task ExecuteAsync();
    }
}