﻿using System;

namespace CanineDotNet.Common
{
    /// <summary>
    /// Localized string definition
    /// </summary>
    public partial class LocalizedString
    {
        public LocalizedString()
        {
        }

        public LocalizedString(Guid key, string text, string description = "")
        {
            this.Key = key;
            this.Text = text;
            this.Description = description;
        }

        public static LocalizedString Create(Guid key, string text, string description = "")
        {
            return new LocalizedString(key, text, description);
        }

        /// <summary>
        /// Gets or sets Lang
        /// </summary>
        public Guid Key { get; set; }

        /// <summary>
        /// Gets or sets Text
        /// </summary>
        public string Text { get; set; }
        public string Description { get; set; }
    }
}