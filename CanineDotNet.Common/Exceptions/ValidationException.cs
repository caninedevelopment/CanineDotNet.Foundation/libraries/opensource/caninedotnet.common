﻿namespace CanineDotNet.Common.Exceptions
{
    public class ValidationException : LocalizedException
    {
        /// <summary>
        /// propertyName should be filled out by using "nameof()"
        /// </summary>
        /// <param name="propertyName">please use nameof()</param>
        public ValidationException(string propertyName) : base("A valid value should be provided for the property " + propertyName)
        {
            Error = LocalizedString.ValidationError(propertyName);
        }

        public ValidationException(string message, string propertyName) : base(message)
        {
            Error = LocalizedString.ValidationError(propertyName);
        }
    }
}