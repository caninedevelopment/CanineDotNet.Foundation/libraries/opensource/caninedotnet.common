﻿namespace CanineDotNet.Common.Interfaces.Command
{
    public interface IDtoRequest
    {
        void Validate();
    }
}