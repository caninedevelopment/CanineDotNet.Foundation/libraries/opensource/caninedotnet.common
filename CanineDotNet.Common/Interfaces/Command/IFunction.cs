﻿namespace CanineDotNet.Common.Interfaces.Command
{
    public interface IFunction<TCommandModelIn, TCommandModelOut> : ICommand
        where TCommandModelIn : IDtoRequest
        where TCommandModelOut : IDtoResponse
    {
        TCommandModelOut Execute(TCommandModelIn input);
    }

    public interface IFunction<TCommandModelOut> : ICommand
    where TCommandModelOut : IDtoResponse
    {
        TCommandModelOut Execute();
    }
}