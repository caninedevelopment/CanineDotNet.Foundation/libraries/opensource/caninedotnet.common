﻿namespace CanineDotNet.Common
{
    /// <summary>
    /// Class that defines how external Id's should be described. 
    /// An external Id hsould always have a PathDefinition that describes where the value comes from.
    /// </summary>
    public class ExternalReference
    {
        /// <summary>
        /// The external systems id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The Path definition of the external system
        /// </summary>
        public PathDefinition Path { get; set; }
        public ExternalReference() { }
        public ExternalReference(string id, PathDefinition path)
        {
            this.Id = id;
            this.Path = path;
        }

        public class PathDefinition
        {
            public enum TypeDefinition
            {
                Rest = 10,
                RabbitMq = 20
            }
            /// <summary>
            /// The Uri (URL for REST and ROUTE for RABBITMQ) to the external system
            /// </summary>
            public string Uri { get; set; }
            /// <summary>
            /// Define the type of URI that is used (REST/RABBITMQ)
            /// </summary>
            public TypeDefinition Type { get; set; }

            public PathDefinition() { }
            public PathDefinition(string uri, TypeDefinition type)
            {
                this.Uri = uri;
                this.Type = type;
            }
        }
    }
}
