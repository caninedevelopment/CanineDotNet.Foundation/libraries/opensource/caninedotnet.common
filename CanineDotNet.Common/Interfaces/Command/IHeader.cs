﻿namespace CanineDotNet.Common.Interfaces.Command
{
    public interface IHeader
    {
        string Token { get; set; }
        string User { get; set; }
    }
}
