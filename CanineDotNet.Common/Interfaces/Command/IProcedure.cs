﻿namespace CanineDotNet.Common.Interfaces.Command
{
    public interface IProcedure<TCommandModelIn> : ICommand
        where TCommandModelIn : IDtoRequest
    {
        void Execute(TCommandModelIn input);
    }

    public interface IProcedure : ICommand
    {
        void Execute();
    }
}