﻿namespace CanineDotNet.Common.Interfaces.Command
{
    public interface IClient
    {
        string InitiatedByIp { get; set; }
    }
}
