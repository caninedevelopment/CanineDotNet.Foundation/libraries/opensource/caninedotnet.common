﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CanineDotNet.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime UtcNow { get; }
        DateTime Now { get; }
    }
}
