﻿using System.Threading.Tasks;

namespace CanineDotNet.Common.Interfaces.Command
{
    public interface IFunctionAsync<TCommandModelIn, TCommandModelOut> : ICommand
        where TCommandModelIn : IDtoRequest
        where TCommandModelOut : IDtoResponse
    {
        Task<TCommandModelOut> ExecuteAsync(TCommandModelIn input);
    }

    public interface IFunctionAsync<TCommandModelOut> : ICommand
    where TCommandModelOut : IDtoResponse
    {
        Task<TCommandModelOut> ExecuteAsync();
    }
}