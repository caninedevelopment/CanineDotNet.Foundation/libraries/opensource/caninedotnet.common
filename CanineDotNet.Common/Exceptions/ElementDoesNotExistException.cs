﻿namespace CanineDotNet.Common.Exceptions
{
    public class ElementDoesNotExistException : LocalizedException
    {
        public ElementDoesNotExistException(string message, string itemId) : base(message)
        {
            Error = LocalizedString.ItemDoesNotExist(itemId);
        }
    }
}
